

-- INSTALLATION --

* Download module from http://drupal.org

* Extract folder in sites/all/modules.

* Goto Administration » Module and install added module


-- CONFIGURATION --

* For configure goto Administration » Configuration » Module Skeleton:

  - Page will open, enter the information and submit the form.

  - After submit you will get zip file. extract its on sites/all/modules location in your system.

